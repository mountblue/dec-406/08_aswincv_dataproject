import mysql.connector
import csv


def find_years(my_cursor):
    query = "select distinct team1 from MATCHES;"
    my_cursor.execute(query)
    teams = []
    teams_temp = my_cursor.fetchall()
    for iterator in teams_temp:
        teams.append(iterator[0])
    return teams


def find_teams(my_cursor):
    query = "select distinct season from MATCHES order by season;"
    my_cursor.execute(query)
    years = []
    years_temp = my_cursor.fetchall()
    for iterator in years_temp:
        years.append(iterator[0])
    return years


def find_win_team(year, teams, my_cursor):
    query = "select winner,count(winner) from MATCHES where season =%s group by winner;", (year,)
    my_cursor.execute("select winner,count(winner) from MATCHES where season =%d group by winner;", year)
    win_team = {}
    for iterator in teams:
        win_team[iterator] = 0
    win_team_temp = my_cursor.fetchall()
    for iterator in win_team_temp:
        win_team[iterator[0]] = iterator[1]
    return list(win_team.values())


def find_year_win_dict(years, my_cursor, teams):
    year_win_dict = {}
    for year in years:
        year_win_dict[year] = find_win_team(year, teams, my_cursor)
    # print(year_win_dict)


def caller_function():
    my_db = mysql.connector.connect(host='localhost',
                                    user='aswin',
                                    passwd='testTEST123#',
                                    database='DATAPROJECT',
                                    auth_plugin='mysql_native_password')
    mycurser = my_db.cursor()
    years = find_years(mycurser)
    teams = find_teams(mycurser)
    find_year_win_dict(years, mycurser, teams)


caller_function()
