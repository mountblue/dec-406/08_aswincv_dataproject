import csv
from matplotlib import pyplot as plt
player_name = "S Dhawan"


def read_file(file_name):
    return csv.DictReader(open(file_name))


def draw(year_runs):
    x_axis = year_runs.keys()
    y_axis = year_runs.values()
    plt.plot(x_axis, y_axis)
    plt.ylabel('runs')
    plt.xlabel('year')
    plt.title('performance of player')
    plt.show()


def find_runs_per_year(year):
    matches = read_file("matches.csv")
    deliveries = read_file("deliveries.csv")
    match_id = []
    runs = 0
    for match in matches:
        if match["season"] == year:
            match_id.append(match["id"])
    for dlv in deliveries:
        if dlv["match_id"] in match_id:
            if dlv["batsman"] == player_name:
                runs += int(dlv["batsman_runs"])
    return runs


def solveProb5():
    year_runs = {}
    years = []
    matches = read_file("matches.csv")
    for match in matches:
        if match["season"] not in years:
            years.append(match["season"])
    years.sort()
    for year in years:
        year_runs[year] = find_runs_per_year(year)
    print(year_runs)
    draw(year_runs)


solveProb5()
