import csv
from matplotlib import pyplot as plt
import operator


def read_file(file_name):
    return csv.DictReader(open(file_name))


def find_matchids(year):
    match_id = []
    matches = read_file("matches.csv")
    for match in matches:
        if match["season"] == year:
            match_id.append(match["id"])
    return match_id


def draw(bowler_economy):
    bowler = []
    y_axis = []
    for iterator in range(0, 11):
        bowler.append(bowler_economy[iterator][0])
        y_axis.append(bowler_economy[iterator][1])
    x_axis = list(range(len(bowler)))
    plt.bar(x_axis, y_axis)
    plt.xticks(x_axis, bowler, rotation=-90)
    plt.ylabel('economy')
    plt.xlabel('player')
    plt.title('bowler ecconomy')
    plt.show()


def find_bowler_ecconomy(year):
    match_id = find_matchids(year)
    bowler_economy = {}
    bowler_ball = {}
    bowler_runs = {}
    deliveries = read_file("deliveries.csv")
    for delivery in deliveries:
        if delivery["match_id"] in match_id:
            if(int(delivery["noball_runs"])==0 and int(delivery["wide_runs"])==0):
                if delivery["bowler"] not in bowler_ball:
                    bowler_ball[delivery["bowler"]] = 1
                else:
                    bowler_ball[delivery["bowler"]] += 1
            if delivery["bowler"] not in bowler_runs:
                bowler_runs[delivery["bowler"]] = int(delivery["total_runs"]) - int(delivery["bye_runs"])
            else:
                bowler_runs[delivery["bowler"]] += int(delivery["total_runs"]) - int(delivery["bye_runs"])
    bowler_list = bowler_ball.keys()
    for bowler in bowler_list:
        bowler_economy[bowler] = bowler_runs[bowler]/(bowler_ball[bowler]/6)
    sorted_economy = sorted(bowler_economy.items(), key=operator.itemgetter(1))
    return sorted_economy


def caller_function():
    bowler_economy = find_bowler_ecconomy("2015")
    draw(bowler_economy)


caller_function()
