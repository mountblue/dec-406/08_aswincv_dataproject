import csv
from matplotlib import pyplot as plt


def read_file(file_name):
    return csv.DictReader(open(file_name))


def draw(x_axis, y_axis):
    team = x_axis
    x_axis = list(range(len(team)))
    plt.bar(x_axis, y_axis)
    plt.xticks(x_axis, team, rotation=-90)
    plt.ylabel('extra runs')
    plt.xlabel('team')
    plt.title('extra runs conceded per team')
    plt.show()


def find_bowler_extra_runs():
    bowler_extra_runs = {}
    match_id_2016 = []
    matches = read_file("matches.csv")
    for match in matches:
        if match["season"] == "2016":
            match_id_2016.append(match["id"])
    deliveries = read_file("deliveries.csv")
    for delivery in deliveries:
        if delivery["match_id"] in match_id_2016:
            bowler = delivery["bowling_team"]
            if bowler in bowler_extra_runs:
                bowler_extra_runs[bowler] += int(delivery["extra_runs"])
            else:
                bowler_extra_runs[bowler] = int(delivery["extra_runs"])
    coordinates = []
    coordinates.append(list(bowler_extra_runs.keys()))
    coordinates.append(list(bowler_extra_runs.values()))
    return coordinates


def caller_function():
    coordinates = find_bowler_extra_runs()
    draw(coordinates[0], coordinates[1])


caller_function()
