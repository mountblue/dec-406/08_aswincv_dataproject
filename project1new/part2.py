import csv
from matplotlib import pyplot as plt
import matplotlib.patches as mpatches


def read_file(file_name):
    return csv.DictReader(open(file_name))


def find_team_win_dict(years, team):
    matchWin = {}
    for year in years:
        matchWin[year] = 0
    matches = read_file("matches.csv")
    for match in matches:
        if match["team1"] == team and match["winner"] == team:
            matchWin[match["season"]] += 1
        elif match["team2"] == team and match["winner"] == team:
            matchWin[match["season"]] += 1
    return list(matchWin.values())


def find_year_team():
    year_team = []
    years = []
    teams = []
    matches = read_file("matches.csv")
    for match in matches:
        if match["team1"] not in teams:
            teams.append(match["team1"])
        if match["team2"] not in teams:
            teams.append(match["team2"])
        if match["season"] not in years:
            years.append(match["season"])
    year_team.append(sorted(years))
    year_team.append(sorted(teams))
    return year_team


def draw(y_axis, years, teams):
    colorList = ['#00FFFF', '#0000FF', '#8A2BE2', '#A52A2A', '#5F9EA0',
                 '#7FFF00', '#6495ED', '#006400', '#FF8C00', '#483D8B',
                 '#00008B', '#D2691E', '#A9A9A9', '#808000']
    iterator = 0
    x_axis = list(range(len(years)))
    data = []
    legend_args = []
    for y in y_axis:
        if len(data) == 0:
            data = y[:]
            legend_temp = plt.bar(x_axis, y, color=colorList[iterator])
        else:
            legend_temp = plt.bar(x_axis, y, bottom=data, color=colorList[iterator])
            data = [data[i]+y[i] for i in range(len(data))]
        legend_args.append(legend_temp)
        iterator += 1
    legend_temp = plt.xticks(x_axis, years, rotation=-90)
    plt.ylabel('no of wins')
    plt.xlabel('team')
    plt.title('matches won of all teams over all the years')
    plt.legend(legend_args, teams, bbox_to_anchor=(1.1, 1.05))
    plt.tight_layout()
    plt.show()


def caller_function():
    team_win_dict = {}
    year_team = find_year_team()
    for team in year_team[1]:
        team_win_dict[team] = find_team_win_dict(year_team[0], team)
    y_axis = team_win_dict.values()
    draw(y_axis, year_team[0], year_team[1])


caller_function()
