import csv
from matplotlib import pyplot as plt


def draw(x, y):
    plt.bar(x, y)
    plt.ylabel('no of matches')
    plt.xlabel('year')
    plt.title('number of matches played per year')
    plt.show()


def read_file(file_name):
    return csv.DictReader(open(file_name))


def find_matches_season():
    match_year = {}
    matches = read_file("matches.csv")
    for match in matches:
        season = match["season"]
        if season in match_year:
            match_year[season] += 1
        else:
            match_year[season] = 1
    return match_year


def caller_function():
    match = find_matches_season()
    x_axis = list(match.keys())
    x_axis.sort()
    y_axis = []
    for iterator in x_axis:
        y_axis.append(match[iterator])
    draw(x_axis, y_axis)


caller_function()
