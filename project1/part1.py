import csv
from matplotlib import pyplot as plt
from matplotlib import style


fileName1 = "matches.csv"
fileName2 = "deliveries.csv"
matches = []
deliveries = []
# reading csv file
with open(fileName1, 'r') as csvfile:
    csvreader = csv.DictReader(csvfile)
    for row in csvreader:
        matches.append(row)
with open(fileName2, 'r') as csvfile:
    csvreader = csv.DictReader(csvfile)
    for row in csvreader:
        deliveries.append(row)


def solveProb1() :
    match = {}
    for i in range(len(matches)):
        season  = matches[i]["season"]
        if season in match :
            match[season] += 1
        else :
            match[season] = 1
    x = list(match.keys())
    x.sort()
    y =[]
    for i in x :
        y.append(match[i])
    plt.bar(x,y)
    plt.ylabel('no of matches')
    plt.xlabel('year')
    plt.title('number of matches played per year')
    plt.show()


solveProb1()
