import csv
from matplotlib import pyplot as plt
player_name = "S Dhawan"


def find_runs_per_year(year,matches,deliveries) :
    runs = 0
    for match in range(len(matches)) :
        if matches[match]["season"] == year :
            for dlv in range(len(deliveries)) :
                if deliveries[dlv]["match_id"] == matches[match]["id"] :
                    if deliveries[dlv]["batsman"] == player_name :
                        runs += int(deliveries[dlv]["batsman_runs"])
    return runs


def draw(year_runs) :
    x_axis = year_runs.keys()
    y_axis = year_runs.values()
    plt.plot(x_axis,y_axis)
    plt.ylabel('runs')
    plt.xlabel('year')
    plt.title('performance of player')
    plt.show()


def solveProb5() :
    year_runs = {}
    fileName1 = "matches.csv"
    fileName2 = "deliveries.csv"
    matches = []
    deliveries = []
    years = []
    with open(fileName1, 'r') as csvfile:
        csvreader = csv.DictReader(csvfile)
        for row in csvreader:
            matches.append(row)
    with open(fileName2, 'r') as csvfile:
        csvreader = csv.DictReader(csvfile)
        for row in csvreader:
            deliveries.append(row)
    for match in range(len(matches)) :
        year = matches[match]["season"]
        if year in years :
            continue
        else :
            years.append(year)
    years.sort()
    for year in years :
        year_runs[year] = find_runs_per_year(year,matches,deliveries)
    draw(year_runs)


solveProb5()
