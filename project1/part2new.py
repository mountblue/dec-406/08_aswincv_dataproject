import csv
from matplotlib import pyplot as plt


fileName1 = "matches.csv"
fileName2 = "deliveries.csv"
matches = []
deliveries = []
# reading csv file
with open(fileName1, 'r') as csvfile:
    csvreader = csv.DictReader(csvfile)
    for row in csvreader:
        matches.append(row)
with open(fileName2, 'r') as csvfile:
    csvreader = csv.DictReader(csvfile)
    for row in csvreader:
        deliveries.append(row)
colorList = ['#FAEBD7','#00FFFF','#000000','#0000FF','#8A2BE2','#A52A2A'
             ,'#5F9EA0','#7FFF00','#6495ED','#006400','#FF8C00','#483D8B'
             ,'#696969','#1E90FF','#ADFF2F']
teamList = []


def draw(y,last) :
    last -= 1
    ind = [i for i in range(1,11)]
    plt.bar(ind, y , color = colorList[last])

def yearwinninglist(yr) :
    matchWin = {}
    for i in range(len(matches)) :
        if matches[i]["season"] == yr :
            player1 = matches[i]["team1"]
            player2 = matches[i]["team2"]
            winner = matches[i]["winner"]
            if player1 == winner :
                if player1 in matchWin :
                    matchWin[player1] += 1
                else :
                    matchWin[player1] = 1
            elif player2 == winner :
                if player2 in matchWin :
                    matchWin[player2] += 1
                else :
                    matchWin[player2] = 1
    y = [value for (key, value) in sorted(matchWin.items())]
    return y
def addition(list1,list2) :
    list3 = []
    for i in range(0,len(list1)) :
        list3.append(list1[i]+list2[i])
    return list3
def solveProb2() :
    last = 15
    years = []
    for i in range(len(matches)) :
        year = matches[i]["season"]
        if year in years :
            continue
        else :
            years.append(year)
    years.sort()
    yaxis = []
    for year in years :
        last -= 1
        if len(yaxis) == 0 :
            yaxis.append(yearwinninglist(year))
            draw(yaxis[-1],last)
        else :
            yaxis.append(yearwinninglist(year))
            draw(addition(yaxis[-1],yaxis[-2]),last)
        print(yaxis[-1])
    plt.show()

solveProb2()
