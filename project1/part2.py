import csv
from matplotlib import pyplot as plt
from matplotlib import style


fileName1 = "matches.csv"
fileName2 = "deliveries.csv"
matches = []
deliveries = []
teams = []
with open(fileName1, 'r') as csvfile:
    csvreader = csv.DictReader(csvfile)
    for row in csvreader:
        matches.append(row)
        if matches[-1]["team1"] in teams :
            continue
        else :
            teams.append(matches[-1]["team1"])
        if matches[-1]["team2"] in teams :
            continue
        else :
            teams.append(matches[-1]["team2"])
with open(fileName2, 'r') as csvfile:
    csvreader = csv.DictReader(csvfile)
    for row in csvreader:
        deliveries.append(row)


def team_win_dict(yr) :
    matchWin = {}
    for i in teams :
        matchWin[i] = 0
    for i in range(len(matches)) :
        if matches[i]["season"] == yr :
            player1 = matches[i]["team1"]
            player2 = matches[i]["team2"]
            winner = matches[i]["winner"]
            if player1 == winner :
                matchWin[player1] += 1
            elif player2 == winner :
                matchWin[player2] += 1
    sortedByValue = sorted(matchWin.items(), key=lambda kv: kv[1])
    return sortedByValue


def solveProb2() :
    x = []
    for i in range(len(matches)) :
        year = matches[i]["season"]
        if year in x :
            continue
        else :
            x.append(year)
    x.sort()
    colorList = ['#00FFFF','#000000','#0000FF','#8A2BE2','#A52A2A'
                 ,'#5F9EA0','#7FFF00','#6495ED','#006400','#FF8C00','#483D8B'
                 ,'#ADFF2F']
    yaxis = []
    for year in x :
        temp = team_win_dict(year)
        win_per_year = []
        for item in temp :
            win_per_year.append(item[1])
        yaxis.append(win_per_year)
    last = len(colorList)-1
    flag = 0
    x_axis = list(range(len(teams)))
    for y in yaxis :
        c = colorList[last]
        last -= 1
        ind = [i for i in range(0,len(y))]
        if flag == 0 :
            flag = 1
            data = y[:]
            plt.bar(x_axis,y, color = c)
            plt.xticks(x_axis,teams,rotation = -90)
            print(teams)
        else :
            plt.bar(x_axis,y ,bottom = data, color = c)
            plt.xticks(x_axis,teams,rotation = -90)
            data = [data[i]+y[i] for i in range(len(data))]
    plt.ylabel('no of wins')
    plt.xlabel('team')
    plt.title('matches won of all teams over all the years')
    plt.show()


solveProb2()
