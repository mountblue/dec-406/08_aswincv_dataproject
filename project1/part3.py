import csv
from matplotlib import pyplot as plt


fileName1 = "matches.csv"
fileName2 = "deliveries.csv"
matches = []
deliveries = []
# reading csv file
with open(fileName1, 'r') as csvfile:
    csvreader = csv.DictReader(csvfile)
    for row in csvreader:
        matches.append(row)
with open(fileName2, 'r') as csvfile:
    csvreader = csv.DictReader(csvfile)
    for row in csvreader:
        deliveries.append(row)


def solveProb3() :
    extra = {}
    for i in range(len(matches)) :
        if matches[i]["season"] == "2016" :
            for j in range(len(deliveries)) :
                if matches[i]["id"] == deliveries[j]["match_id"] :
                    bowler = deliveries[j]["bowling_team"]
                    if bowler in extra :
                        extra[bowler] += int(deliveries[j]["extra_runs"])
                    else :
                        extra[bowler] = int(deliveries[j]["extra_runs"])
    x = extra.keys()
    print(x)
    y =[]
    ind = [i+1 for i in range(2000)]
    for i in x :
        y.append(extra[i])
    # plt.xticks(x,y,rotation = -90)
    plt.bar(x, y)
    plt.ylabel('extra runs')
    plt.xlabel('team')
    plt.title('extra runs conceded per team')
    plt.show()

    
solveProb3()
