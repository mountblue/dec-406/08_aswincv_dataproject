import csv
from matplotlib import pyplot as plt
from itertools import islice


fileName1 = "matches.csv"
fileName2 = "deliveries.csv"
matches = []
deliveries = []
with open(fileName1, 'r') as csvfile:
    csvreader = csv.DictReader(csvfile)
    for row in csvreader:
        matches.append(row)
with open(fileName2, 'r') as csvfile:
    csvreader = csv.DictReader(csvfile)
    for row in csvreader:
        deliveries.append(row)
bowler_runs = {}
bowler_over = {}


def take(n, iterable):
    "Return first n items of the iterable as a list"
    return list(islice(iterable, n))


def find_over_runs(id,bowler,itr) :
        over = deliveries[itr]["over"]
        while(deliveries[itr]["over"] == over) :
            if bowler in bowler_runs :
                bowler_runs[bowler] += int(deliveries[itr]["total_runs"])-int(deliveries[itr]["bye_runs"])
            else :
                bowler_runs[bowler] = int(deliveries[itr]["total_runs"])-int(deliveries[itr]["bye_runs"])
            itr += 1
        if bowler in bowler_over :
            bowler_over[bowler] += 1
        else :
            bowler_over[bowler] = 1


def solveProb4() :
    bowler_economy = {}
    for i in range(len(matches)) :
        if matches[i]["season"] =="2015" :
            for j in range(len(deliveries)) :
                if deliveries[j]["match_id"] == matches[i]["id"] :
                    bowler = deliveries[j]["bowler"]
                    find_over_runs(matches[i]["id"],bowler,j)
    bowler_list = bowler_runs.keys()
    for bowler in bowler_list :
        bowler_economy[bowler] = bowler_runs[bowler]/bowler_over[bowler]
    plt.xticks(rotation = -90)
    sortedByValue = sorted(bowler_economy.items(), key=lambda kv: kv[1])
    x = []
    y = []
    for i in range(10) :
        x.append(sortedByValue[i][0])
        y.append(sortedByValue[i][1])
    plt.bar(x,y)
    plt.ylabel('economy')
    plt.xlabel('bowler')
    plt.title('top economical bowlers of 2015')
    plt.show()


solveProb4()
